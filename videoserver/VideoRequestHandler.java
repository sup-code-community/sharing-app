import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URI;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;

 class VideoRequestHandler implements HttpHandler{
    @Override
    public void handle(HttpExchange t ) {
        try{
            URI uri = t.getRequestURI();
            t.getResponseHeaders().set("Content-Type","video/mp4");
            OutputStream os = t.getResponseBody();
            //we have to match the id to a certain path to complete 
            String videoId = ParseVideoQuery.createResponseFromQueryParams(uri.getQuery());
            System.out.println("videoId: "+videoId);

            //TODO:map the videoId to the path of the video
            //???????????????????????????????????????????????????????
            //???????????????????????????????????????????????????????
            //we have to generate the path from the certain id
            File file = new File("/home/ivan/Téléchargements/Drones, robots, mobile payments - Africa's growing digital business - DW Documentary.mp4");
            t.sendResponseHeaders(200,file.length());
            byte[] buff = new byte[1024];
            FileInputStream fs = new FileInputStream(file);

            //we launch into new thread to permit to the server to serve multiple persons
            Thread sendVideo = new Thread(new Runnable() {
            
                @Override
                public void run() {
                    try{
                        int count = 0;
                        while((count=fs.read(buff))>=0){
                            os.write(buff,0,count);
                        }
                    }
                    catch(Exception e ){
                        e.printStackTrace();
                    }
                }
            });

           sendVideo.start();
           //end of code of process serving


        }catch(Exception e){
            e.printStackTrace();
        }

    }

}