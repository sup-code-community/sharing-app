import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import java.net.InetSocketAddress;;


public class VideoServer{
    private int PORT;
    private HttpServer server;

    public VideoServer(int PORT){

        this.PORT = PORT;
    }

    public void startVideoServer() throws Exception{
        server = HttpServer.create(new InetSocketAddress(this.PORT),0);
        server.createContext("/stream",new VideoRequestHandler());
        server.setExecutor(null);
        server.start();
    }


}