import java.net.URI;

public class ParseVideoQuery{
    public static String createResponseFromQueryParams(String parameters) {
        String[] result =  parameters.split("=");
        if(result[0].contains("videoId")){
            return result[1];
        }else{
            return null;
        }
    }
}