public class OSType {
    public static final int WINDOWS_TYPE = 0;
    public static final int LINUX_TYPE = 1;

    public static int getOSType(){
        if(System.getProperties().getProperty("os.name").toLowerCase().contains("linux")){
            return 1;
        }
        if(System.getProperties().getProperty("os.name").toLowerCase().contains("windows")) {
            return 0;
        }
        return -1;
    }
}
