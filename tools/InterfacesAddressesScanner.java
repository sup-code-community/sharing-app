import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;

//help to get all the broadcast addresses and ip address of the active network interfaces
public class InterfacesAddressesScanner {
    String IpAddress ;
    String BroadcastAddress;
    HashMap<String,String> addresses;

    public HashMap<String,String> listAllAddresses() throws Exception{
        addresses = new HashMap<>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while(interfaces.hasMoreElements()){
            NetworkInterface networkInterface = interfaces.nextElement();
            if(networkInterface.isLoopback() || !networkInterface.isUp()){
                continue;
            }

            if(OSType.getOSType()==1) {
                IpAddress = networkInterface.getInterfaceAddresses().get(1).getAddress().getCanonicalHostName();
                BroadcastAddress = networkInterface.getInterfaceAddresses().get(1).getBroadcast().getCanonicalHostName();
            }else if(OSType.getOSType()==0){
                IpAddress = networkInterface.getInterfaceAddresses().get(0).getAddress().getHostAddress();
                BroadcastAddress = networkInterface.getInterfaceAddresses().get(0).getBroadcast().getHostAddress();
            }
            addresses.put(IpAddress,BroadcastAddress);
        }
        return addresses;
    }
}
