import com.google.gson.JsonObject;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class VisibilityClientProcess {
    private HashMap<String,String> addresses ;
    InterfacesAddressesScanner interfacesAddressesScanner = new InterfacesAddressesScanner();
    JsonObject visibilityFrame;
    VisibilityClient visibilityClient;
    private boolean run = false;

    //hostname is the name of the host witch will be visible on the network
    //PORT is the port for udp diffusion

    public void launchClientProcess(String hostName,int PORT) throws Exception{
        run = true;
        while(run){
            //first we take all the available addresses
            addresses = interfacesAddressesScanner.listAllAddresses();
            visibilityClient = new VisibilityClient();

            for(Map.Entry<String,String> element:addresses.entrySet()){
                visibilityFrame = VisibilityFrame.getAsJson(hostName,element.getKey(),ServiceType.getVisibilityType());
                visibilityClient.broadcast(visibilityFrame.getAsJsonObject().toString(), InetAddress.getByName(element.getValue()),PORT);
            }

        }

    }

    public void stop(){
        run = false;
    }
}
