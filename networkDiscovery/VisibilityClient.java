//contains udp client to diffuse presence

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class VisibilityClient {
    private DatagramSocket socket;

    public VisibilityClient() throws Exception{
        socket = new DatagramSocket();
        socket.setBroadcast(true);
    }

    public void broadcast(String broadcastMessage, InetAddress broadcastAddress,int PORT)throws Exception{
        byte[] buffer = broadcastMessage.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length,broadcastAddress,PORT);
        socket.send(packet);
    }

    public void stop(){
        socket.close();
    }

}