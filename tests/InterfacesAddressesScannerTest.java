import java.util.HashMap;
import java.util.Map;

public class InterfacesAddressesScannerTest {
    public static void main(String[] args) throws Exception {
        InterfacesAddressesScanner interfacesAddressesScanner  = new InterfacesAddressesScanner();
        HashMap<String,String> addresses = interfacesAddressesScanner.listAllAddresses();
        for(Map.Entry<String,String> element:addresses.entrySet()){
            System.out.println(element.getKey());
            System.out.println(element.getValue());
        }
    }
}
