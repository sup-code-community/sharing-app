import java.io.File;

public class FileExplorerTest{
    public static void main(String args[]){
        FileExplorer fileExplorer  = new FileExplorer("");
        File[] files = fileExplorer.getCurrentFiles();
        int i = 0;
        fileExplorer.chooseFile(0, FileExplorer.ENTER);
        files = fileExplorer.getCurrentFiles();
        while(i <files.length){
            System.out.println(i+" "+files[i].getAbsolutePath());
            i++;
        }
        System.out.println("switching again");
        fileExplorer.chooseFile(19, FileExplorer.ENTER);
        System.out.println("ok");
        files = fileExplorer.getCurrentFiles();
        i = 0;
        while(i <files.length){
            System.out.println(files[i].getAbsolutePath());
            i++;
        }

        System.out.println(fileExplorer.chooseFile(0, FileExplorer.SELECT));
    }
}