import com.google.gson.JsonObject;

public class VisibilityFrame {
    public static JsonObject getAsJson(String hostName,String ipAddress,int serviceType){
        JsonObject visibilityFrame = new JsonObject();
        visibilityFrame.addProperty("hostName",hostName);
        visibilityFrame.addProperty("ipAddress",ipAddress);
        visibilityFrame.addProperty("serviceType",serviceType);
        return visibilityFrame;
    }
}
