import java.io.File;

public class FileExplorer{
    //stores the current files of the directory
    private File[] files;

    //stores the current working directory
    private File file;

    public static final int SELECT = 0;
    public static final int ENTER = 1;

    private String defaultPath;


    //constructor to set the default path
    
    public FileExplorer(String defaultPath){
        if(defaultPath.equals("")){
            //we choose the roots storage of the system
            this.files = File.listRoots();
        }
        else{
            //we directly choose the default path 
            
            //check if the path provided is a directory
            if(file.isDirectory()){
                this.defaultPath = defaultPath;
                file = new File(this.defaultPath);
                files = file.listFiles();
            }

            else{
                this.defaultPath="";
                files = File.listRoots();
            }
        }
    }


    
    //to get the list of files of the working directory
    public File[] getCurrentFiles(){
        return this.files;
    }

    //the choice number is just the index of the file in the array
    //the type number is just the type of choice , 0 to enter in the file,1 to get the absolute path of the file
    public String chooseFile(int choice,int type){
        if(type==SELECT && (choice>=0 && choice<this.files.length)){
            return files[choice].getAbsolutePath();
        }
        else if(type==ENTER && choice<this.files.length){
            if(files[choice].isDirectory()){
                file = files[choice];
                files = file.listFiles();
                return null;
            }
            else{
                return null;
            }
        }
        return null;
    }

}